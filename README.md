# Logging php

> This package is build to emit error & request logs from different Quince Platform modules over rabbitmq using carrot package and the official rabbitmq php library
[php-amqplib/php-amqplib](https://github.com/php-amqplib/php-amqplib)

### Required extension
 - bcmath

```bash
# Docker install
RUN docker-php-ext-install bcmath
```


## Installation

```bash
# Require this package with composer:
composer require quince/logging-php
```

## Laravel
```bash
# Add service provider to config/app.php
"Quince\Logging\LogsEmitterServiceProvider::class"
# Run artisan command to add configuration file
php artisan vendor:publish
```

## Lumen
```bash
# Register a Provider in bootstrap/app.php
$app->register(Quince\Logging\LogsEmitterServiceProvider::class);
# Add configuration file to config/logging-module.php
You can find the initial file in the config directory of the package
```


## Add middleware
>This package provides middleware to log or to ignore your request logs.
By default, there will be no middleware assinged since v0.2.0

Capture all the request logs:

``` ['middleware' => 'requestLogger'] ```

Ignore route from logging 

``` ['middleware' => 'doNotLog'] ```

See more details about lumen middleware! 

> [https://lumen.laravel.com/docs/5.7/middleware]

## Configure
```bash
# Use your environment configuration and set values for your module
QUINCE_HOSTNAME=
QUINCE_MODULE=
QUINCE_INSTANCE=
QUINCE_APP_VERSION=
```


```bash
# Set connection to your rabbitmq server in your env file
CARROT_HOST=
CARROT_PORT=
CARROT_EXCHANGE=
```

> For excluding specific errors please open a configuration file `config/logging-module.php`.

## How to test

There is no tests defined for this repo
