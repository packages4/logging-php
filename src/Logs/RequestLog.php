<?php

namespace Quince\Logging\Logs;

use Quince\Logging\Messages\CarrotEmitter as Carrot;
use Nanotime\Nanotime;
use Quince\Logging\Lib\Uuid;

class RequestLog
{
    /**
     * Keep track of application time snapshots
     * @param array
     */
    protected static $timers = [];

    /**
     * Store the error log entry for exception thrown
     */
    protected static $error;

    /**
     * Uuid used for updating error_id in request_log table
     */
    protected static $uuid;

    /**
     * Store the number of SQL queries executed
     */
    protected static $sqlCount = 0;

    /**
     * Store the total duration, in milliseconds, of all SQL queries executed
     */
    protected static $sqlDuration = 0;

    /**
     * Mark start time
     */
    public static function start()
    {
        static::$uuid = Uuid::v4();

        if (! isset(static::$timers['start'])) {
            static::$timers['start'] = Nanotime::now();
        }

        app('db')->listen(function ($query) {
            static::$sqlCount += 1;
            static::$sqlDuration += $query->time;
        });
    }

    /**
     * Mark end of request processing
     */
    public static function end()
    {
        if (! isset(static::$timers['end'])) {
            static::$timers['end'] = Nanotime::now();
        }
    }

    /**
     * Check if already logged the requested, by seeing if end timer set
     */
    public static function hasFinished()
    {
        return isset(static::$timers['end']);
    }

    /**
     * Prepare new reqeust object and send to carrot wrapper
     * @param  Response $response  The response object
     * @param  Exception $exception Exception object thrown
     */
    public static function finish($request, $response)
    {
        // if last segment is integer then we will assum that we have id_param
        $id_param = (integer) last($request->segments());

        // cast and check if it is real id
        if ($id_param <= 0) {
            $id_param = null;
        }

        $log = [
            'hostname' => config('logging-module.localHost', ''),
            'module' => config('logging-module.module', ''),
            'instance' => config('logging-module.instance', ''),
            'app_version' => config('logging-module.appVersion', ''),
            'id_param' => $id_param,
            'controller' => null,
            'action' => null,
            'user_id' => null,
            'error_code' => null,
            'error_id' => null,
            'sql_count' => 0,
            'sql_duration' => 0,
            'memory_usage' => 0,
        ];

        $route = $request->route();
        if (method_exists($route, 'getAction')) {
            $action = $route->getAction();
        } elseif (is_array($route) && isset($route[1]['uses'])) {
            $action = explode('@', $route[1]['uses']);
        }

        if (isset($action[0])) {
            $log['controller'] = $action[0];
        }

        if (isset($action[1])) {
            $log['action'] = $action[1];
        }

        $log['ip'] = $request->getClientIp();
        $log['url'] = $request->path();
        $log['query'] = $request->getQueryString();
        $log['method'] = $request->getMethod();
        $log['headers'] = $request->headers->all();
        $log['user_agent'] = $request->headers->get('User-Agent');
        $log['memory_usage'] = memory_get_peak_usage();
        $log['sql_count'] = static::$sqlCount;
        $log['sql_duration'] = static::$sqlDuration;

        //Just in case (ie exceptions) mark end to get a nice duration
        if (empty(static::$timers['end'])) {
            self::end();
        }

        if (isset(static::$timers['start'])) {
            $log['total_duration'] = static::$timers['end']->diff(static::$timers['start'])->nanotime();
        }

        if (isset(static::$error)) {
            $log['is_error'] = true;
            $log['error_code'] = static::$error['stack_trace_hash'];
        }

        $log['status_code'] = $response->getStatusCode();
        // pass uuid trough options
        $options['uuid'] = static::$uuid;

        // $log will be emitted over rabbitmq
        Carrot::emit('platform.log.request', $log, $options);
    }

    /**
     * The exception handler will use this method to save the exception
     * @param  Exception $e
     * @return void
     */
    public static function exception($e)
    {
        $errorLog = ErrorLog::getErrorAndEmit($e, static::$uuid);
        static::$error = $errorLog;
    }
}
