<?php

namespace Quince\Logging\Logs;

use Quince\Logging\Messages\CarrotEmitter as Carrot;

class ErrorLog
{
    public static function getErrorAndEmit($exception, $requestUuid = null)
    {
        $hash = sha1((string) $exception);
        $hash = substr($hash, 0, 8);

        $errorLog = [
            'exception_class' => get_class($exception),
            'exception_message' => $exception->getMessage(),
            'stack_trace_hash' => $hash,
            'stack_trace' => (string) $exception,
            'request_uuid' => $requestUuid
        ];

        Carrot::emit('platform.log.error', $errorLog);

        return $errorLog;
    }
}
