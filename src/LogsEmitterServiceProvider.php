<?php

namespace Quince\Logging;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;
use Quince\Carrot\Correlation;
use Quince\Logging\Exceptions\Handler as ErrorHandler;
use Quince\Logging\Lib\Uuid;

class LogsEmitterServiceProvider extends ServiceProvider
{
    /**
     * App istance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * Check if app is lumen.
     *
     * @var bool
     */
    protected $is_lumen = false;

    /**
     * @param Application $app
     */
    public function __construct($app = null)
    {
        if (!$app) {
            $app = app();
        }

        $this->app = $app;
        $this->version = $app->version();
        $this->is_lumen = str_contains($this->version, 'Lumen');
    }

    /**
     * Bootstrap the application.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->is_lumen) {
            $this->app->configure('logging-module');
        } else {
            $this->publishes([
                __DIR__ . '/config/logging-module.php' => config_path('logging-module.php'),
            ]);
            $this->mergeConfigFrom(
                __DIR__ . '/config/logging-module.php', 'logging-module'
            );
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //initialize unique correlation id
        Correlation::setCorrelationId(Uuid::v4());

        $this->registerRequestLogMiddleWare();
        $this->registerExceptionHandler();
    }

    /**
     * Register package middlewares
     *
     * @return void
     */
    protected function registerRequestLogMiddleWare()
    {
        if($this->is_lumen) {
            $this->app->routeMiddleware([
                'doNotLog' => Middleware\DoNotLog::class,
                'requestLogger' => Middleware\RequestLoggerMiddleware::class,
            ]);
        } else {
            $this->app['router']->aliasMiddleware('doNotLog' , Middleware\DoNotLog::class);
            $this->app['router']->aliasMiddleware('requestLogger' , Middleware\RequestLoggerMiddleware::class);
        }
    }

    /**
     * Register handler for capturing all exeptions logs
     *
     * @return void
     */
    protected function registerExceptionHandler()
    {
        $this->app->extend(ExceptionHandler::class, function ($handler, $app) {
            return new ErrorHandler($handler, $app);
        });
    }
}
