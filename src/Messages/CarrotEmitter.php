<?php

namespace Quince\Logging\Messages;

use Quince\Carrot\Carrot;
use Quince\Carrot\Correlation;
use Quince\Logging\Lib\Uuid;
use Nanotime\Nanotime;

class CarrotEmitter
{
    /**
     * Emit logs throuhgt RabbitMQ using carrot-php package
     * @param string $topic
     * @param array $payload
     * @param array $options
     * @return void
    */
    public static function emit($topic, $payload, $options = [])
    {
        // Passed options
        $options['correlation_id'] = Correlation::getCorrelationId();
        $options['uuid'] = $options['uuid'] ?? Uuid::v4();

        $payload['created_at'] = Nanotime::now()->nanotime();

        // get carrot default config
        $config = (array) config('logging-module.carrot', []);

        // override default carrot exchange in case that module want to emit own
        $config['exchange'] = $options['exchange'] ?? ($config['exchange'] ?? null);

        // module config
        $config['localHost'] = config('logging-module.localHost', '');
        $config['instance'] = config('logging-module.instance', '');
        $config['module'] = config('logging-module.module', '');
        $config['appVersion'] = config('logging-module.appVersion', '');

        $carrot = new Carrot($config);
        $carrot->emit($topic, json_encode($payload), $options);
    }
}
