<?php

namespace Quince\Logging\Middleware;

use Closure;

class DoNotLog
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->attributes->add(['requestLogger.doNotLog' => true]);

        return $next($request);
    }
}
