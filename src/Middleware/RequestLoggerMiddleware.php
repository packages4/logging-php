<?php

namespace Quince\Logging\Middleware;

use Closure;
use Quince\Logging\Logs\RequestLog;

class RequestLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->attributes->get('requestLogger.doNotLog')) {
            return $next($request);
        }

        RequestLog::start();

        $response = $next($request);

        // Maybe the exception handler already finished the request
        if (!RequestLog::hasFinished()) {
            RequestLog::end();
            RequestLog::finish($request, $response);
        }

        return $response;
    }
}
