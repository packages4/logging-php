<?php

namespace Quince\Logging\Exceptions;

use Exception;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Quince\Logging\Logs\RequestLog;
use Symfony\Component\Console\Application as ConsoleApplication;

class Handler implements ExceptionHandler
{
    /**
     * Exeption handlers to be excluded
     * @var array
     */
    protected $dontReport = [];

    /**
     * @var Illuminate\Contracts\Debug\ExceptionHandler
     * $handler
     * Laravel default exception handler.
     */
    protected $handler;

    /**
     * Set the dependencie.
     *
     * @param Illuminate\Contracts\Debug\ExceptionHandler    $handler
     * @return void
     */
    public function __construct(ExceptionHandler $handler)
    {
        $this->handler  = $handler;
        $this->dontReport = config('logging-module.dontReportErrors', $this->dontReport);
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        $this->handler->report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \Exception  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->shouldReport($e)) {
            RequestLog::exception($e);
        }

        $response = $this->handler->render($request, $e);

        // Because some cases an exception may not follow through a middleware, call it here
        if (!RequestLog::hasFinished()) {
            RequestLog::end();
            RequestLog::finish($request, $response);
        }

        return $response;
    }

    /**
     * Render an exception to the console.
     *
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @param  \Exception  $e
     * @return void
     */
    public function renderForConsole($output, Exception $e)
    {
        (new ConsoleApplication)->renderException($e, $output);
    }

    /**
     * Determine if the exception should be reported.
     *
     * @param  \Exception  $e
     * @return bool
     */
    public function shouldReport(Exception $e)
    {
        return ! $this->shouldntReport($e);
    }

    /**
     * Determine if the exception is in the "do not report" list.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function shouldntReport(Exception $e)
    {
        if (!is_array($this->dontReport)) {
            return false;
        }

        foreach ($this->dontReport as $type) {
            if ($e instanceof $type) {
                return true;
            }
        }

        return false;
    }
}
