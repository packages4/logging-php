<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Quince platform modules configuration
    |--------------------------------------------------------------------------
    | Hostname : example.com
    | Module   : logging-module
    | Istance  : instance.example
    |--------------------------------------------------------------------------
    | dontReportErrors
    |--------------------------------------------------------------------------
    | List of exeptions to be excluded!
    | ex.
    | 'Illuminate\Auth\Access\AuthorizationException'
    | 'Symfony\Component\HttpKernel\Exception\HttpException'
    | 'Illuminate\Database\Eloquent\ModelNotFoundException'
    | 'Illuminate\Validation\ValidationException'
    | 'Illuminate\Session\TokenMismatchException'
    |
    |--------------------------------------------------------------------------
    | carrot
    |--------------------------------------------------------------------------
    | Carrot settings
    */
    'localHost' => env('QUINCE_HOSTNAME', 'example.com'),
    'module' => env('QUINCE_MODULE', 'module.platform'),
    'instance' => env('QUINCE_INSTANCE', 'Instance.example'),
    'appVersion' => env('QUINCE_APP_VERSION', '1.0.0'),
    'dontReportErrors' => [
        'Illuminate\Auth\Access\AuthorizationException',
        'Illuminate\Validation\ValidationException',
        'Illuminate\Session\TokenMismatchException',
        'Illuminate\Database\Eloquent\ModelNotFoundException',
    ],
    'carrot' => [
        'host' => env('CARROT_HOST', 'localhost'),
        'port' => env('CARROT_PORT', 5672),
        'exchange' => env('CARROT_EXCHANGE', 'platform_logging'),
        'logging' => env('CARROT_LOGGING', true),
    ],
];
